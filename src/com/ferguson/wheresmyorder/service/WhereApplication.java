package com.ferguson.wheresmyorder.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class WhereApplication extends Application {

}
