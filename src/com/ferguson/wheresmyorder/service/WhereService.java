package com.ferguson.wheresmyorder.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ferguson.wheresmyorder.model.Order;

@Path("/where")
@Produces(MediaType.APPLICATION_JSON)
public class WhereService {

    @GET
    @Path("/{id}")
    public Order getOrder(@PathParam("id") final String id) {

        final Order response = new Order();
        response.setId(id);
        response.setStatus("shipped");
        response.setThingThree("batman");

        return response;
    }
}