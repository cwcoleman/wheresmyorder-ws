package com.ferguson.wheresmyorder.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "order")
public class Order {
    private String id;
    private String status;
    private String thingThree;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getThingThree() {
        return thingThree;
    }

    public void setThingThree(final String thingThree) {
        this.thingThree = thingThree;
    }

    @Override
    public String toString() {
        return id + "::" + status + "::" + thingThree;
    }

}
